<?php

return [
    C::POOL_ADMIN_CREATE_OPERATOR."_OP" => 'ایجاد اپراتور جدید',
    C::POOL_ADMIN_CREATE_OPERATOR => 'اپراتور با آی‌دی :id و نام :name :lastname را ایجاد کرد.',

    C::POOL_ADMIN_UPDATE_OPERATOR."_OP" => 'ویرایش اپراتور',
    C::POOL_ADMIN_UPDATE_OPERATOR => 'اپراتور با آی‌دی :id و نام :name :lastname را ویرایش کرد.',

    C::POOL_ADMIN_DELETE_OPERATOR."_OP" => 'حذف اپراتور',
    C::POOL_ADMIN_DELETE_OPERATOR => 'اپراتور با آی‌دی :id و نام :name :lastname را حذف کرد.',

    C::POOL_ADMIN_UPDATE_PROFILE."_OP" => 'ویرایش پروفایل',
    C::POOL_ADMIN_UPDATE_PROFILE => '',

    C::POOL_ADMIN_LOGIN."_OP" => 'ورود',
    C::POOL_ADMIN_LOGIN => '',

    C::POOL_ADMIN_LOGOUT."_OP" => 'خروج',
    C::POOL_ADMIN_LOGOUT => '',
];
