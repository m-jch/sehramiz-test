<?php

return [
    C::ADMIN_LOGIN."_OP" => 'ورود',

    C::ADMIN_LOGOUT."_OP" => 'خروج',

    C::ADMIN_CREATE_SAENS."_OP" => 'ایجاد سانس جدید',
    C::ADMIN_CREATE_SAENS => 'سانس با آی‌دی :id و نام :name در استخر :pool را ایجاد کرد.',

    C::ADMIN_UPDATE_SAENS."_OP" => 'ویرایش سانس',
    C::ADMIN_UPDATE_SAENS => 'سانس با آی‌دی :id و نام :name در استخر :pool را ویرایش کرد.',

    C::ADMIN_DELETE_SAENS."_OP" => 'حذف سانس',
    C::ADMIN_DELETE_SAENS => 'سانس با آی‌دی :id و نام :name در استخر :pool را حذف کرد.',

    C::ADMIN_CREATE_DISCOUNT."_OP" => 'ایجاد تخفیف جدید',
    C::ADMIN_CREATE_DISCOUNT => 'تخفیف با آی‌دی :id و نام :name ایجاد کرد.',

    C::ADMIN_UPDATE_DISCOUNT."_OP" => 'ویرایش تخفیف',
    C::ADMIN_UPDATE_DISCOUNT => 'تخفیف با آی‌دی :id و نام :name ویرایش کرد.',

    C::ADMIN_DELETE_DISCOUNT."_OP" => 'حذف تخفیف',
    C::ADMIN_DELETE_DISCOUNT => 'تخفیف با آی‌دی :id و نام :name حذف کرد.',

    C::ADMIN_CREATE_POOL."_OP" => 'ایجاد مجموعه جدید',
    C::ADMIN_CREATE_POOL => 'مجموعه با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_POOL."_OP" => 'ویرایش مجموعه',
    C::ADMIN_UPDATE_POOL => 'مجموعه با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_POOL."_OP" => 'حذف مجموعه',
    C::ADMIN_DELETE_POOL => 'مجموعه با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_USER."_OP" => 'ایجاد کاربر جدید',
    C::ADMIN_CREATE_USER => 'کاربر با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_USER."_OP" => 'ویرایش کاربر',
    C::ADMIN_UPDATE_USER => 'کاربر با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_USER."_OP" => 'حذف کاربر',
    C::ADMIN_DELETE_USER => 'کاربر با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_POOL_ADMIN."_OP" => 'ایجاد مدیر مجموعه جدید',
    C::ADMIN_CREATE_POOL_ADMIN => 'مدیر مجموعه :pool را با آی‌دی :id و نام :name ایجاد کرد.',

    C::ADMIN_UPDATE_POOL_ADMIN."_OP" => 'ویرایش مدیر مجموعه',
    C::ADMIN_UPDATE_POOL_ADMIN => 'مدیر مجموعه :pool را با آی‌دی :id و نام :name ویرایش کرد.',

    C::ADMIN_DELETE_POOL_ADMIN."_OP" => 'حذف مدیر مجموعه',
    C::ADMIN_DELETE_POOL_ADMIN => 'مدیر مجموعه :pool را با آی‌دی :id و نام :name حذف کرد.',

    C::ADMIN_CREATE_OPERATOR."_OP" => 'ایجاد اپراتور جدید',
    C::ADMIN_CREATE_OPERATOR => 'اپراتور مجموعه :pool را با آی‌دی :id و نام :name ایجاد کرد.',

    C::ADMIN_UPDATE_OPERATOR."_OP" => 'ویرایش اپراتور',
    C::ADMIN_UPDATE_OPERATOR => 'اپراتور مجموعه :pool را با آی‌دی :id و نام :name ویرایش کرد.',

    C::ADMIN_DELETE_OPERATOR."_OP" => 'حذف اپراتور',
    C::ADMIN_DELETE_OPERATOR => 'اپراتور مجموعه :pool را با آی‌دی :id و نام :name حذف کرد.',

    C::ADMIN_CREATE_ADMIN."_OP" => 'ایجاد ادمین جدید',
    C::ADMIN_CREATE_ADMIN => 'ادمین با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_ADMIN."_OP" => 'ویرایش ادمین',
    C::ADMIN_UPDATE_ADMIN => 'ادمین با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_ADMIN."_OP" => 'حذف ادمین',
    C::ADMIN_DELETE_ADMIN => 'ادمین با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_SLIDER."_OP" => 'ایجاد اسلایدر جدید',
    C::ADMIN_CREATE_SLIDER => 'اسلایدر با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_SLIDER."_OP" => 'ویرایش اسلایدر',
    C::ADMIN_UPDATE_SLIDER => 'اسلایدر با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_SLIDER."_OP" => 'حذف اسلایدر',
    C::ADMIN_DELETE_SLIDER => 'اسلایدر با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_ADS."_OP" => 'ایجاد تبلیغات جدید',
    C::ADMIN_CREATE_ADS => 'تبلیغات با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_ADS."_OP" => 'ویرایش تبلیغات',
    C::ADMIN_UPDATE_ADS => 'تبلیغات با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_ADS."_OP" => 'حذف تبلیغات',
    C::ADMIN_DELETE_ADS => 'تبلیغات با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_PAGE."_OP" => 'ایجاد صفحه جدید',
    C::ADMIN_CREATE_PAGE => 'صفحه با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_PAGE."_OP" => 'ویرایش صفحه',
    C::ADMIN_UPDATE_PAGE => 'صفحه با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_PAGE."_OP" => 'حذف صفحه',
    C::ADMIN_DELETE_PAGE => 'صفحه با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_UPDATE_PAGE_CONTACT."_OP" => 'ویرایش صفحه تماس با ما',

    C::ADMIN_SEND_RESPONSE_CONTACT."_OP" => 'پاسخ به تماس با ما',
    C::ADMIN_SEND_RESPONSE_CONTACT => 'به یک تماس با ما با آی‌دی :id و ایمیل :email پاسخ داد.',

    C::ADMIN_DELETE_CONTACT."_OP" => 'حذف یک تماس با ما',
    C::ADMIN_DELETE_CONTACT => 'تماس با ما با آی‌دی :id و ایمیل :email را حذف کرد.',

    C::ADMIN_UPDATE_GENERAL_SETTINGS."_OP" => 'ویرایش تنظیمات عمومی',

    C::ADMIN_CREATE_ADS_POSITION."_OP" => 'ایجاد جایگاه تبلیغات جدید',
    C::ADMIN_CREATE_ADS_POSITION => 'جایگاه تبلیغات با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_ADS_POSITION."_OP" => 'ویرایش جایگاه تبلیغات',
    C::ADMIN_UPDATE_ADS_POSITION => 'جایگاه تبلیغات با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_ADS_POSITION."_OP" => 'حذف جایگاه تبلیغات',
    C::ADMIN_DELETE_ADS_POSITION => 'جایگاه تبلیغات با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_TYPE."_OP" => 'ایجاد انواع مجموعه جدید',
    C::ADMIN_CREATE_TYPE => 'نوع مجموعه با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_TYPE."_OP" => 'ویرایش انواع مجموعه',
    C::ADMIN_UPDATE_TYPE => 'نوع مجموعه با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_TYPE."_OP" => 'حذف انواع مجموعه',
    C::ADMIN_DELETE_TYPE => 'نوع مجموعه با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_MAIN_USER_GROUP."_OP" => 'ایجاد گروه کاربری جدید',
    C::ADMIN_CREATE_MAIN_USER_GROUP => 'گروه کاربری با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_MAIN_USER_GROUP."_OP" => 'ویرایش گروه کاربری',
    C::ADMIN_UPDATE_MAIN_USER_GROUP => 'گروه کاربری با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_MAIN_USER_GROUP."_OP" => 'حذف گروه کاربری',
    C::ADMIN_DELETE_MAIN_USER_GROUP => 'گروه کاربری با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_ATTRIBUTE."_OP" => 'ایجاد ویژگی جدید',
    C::ADMIN_CREATE_ATTRIBUTE => 'ویژگی با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_ATTRIBUTE."_OP" => 'ویرایش ویژگی',
    C::ADMIN_UPDATE_ATTRIBUTE => 'ویژگی با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_ATTRIBUTE."_OP" => 'حذف ویژگی',
    C::ADMIN_DELETE_ATTRIBUTE => 'ویژگی با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_UPDATE_SMS_SETTINGS."_OP" => 'ویرایش تنظیمات اس ام اس',
    C::ADMIN_UPDATE_SMS_SETTINGS => '',

    C::ADMIN_SMS_SEND."_OP" => 'ارسال اس ام اس',
    C::ADMIN_SMS_SEND => '',

    C::ADMIN_UPDATE_EMAIL_SETTINGS."_OP" => 'ویرایش تنظیمات ایمیل',
    C::ADMIN_UPDATE_EMAIL_SETTINGS => '',

    C::ADMIN_CREATE_STATE."_OP" => 'ایجاد استان جدید',
    C::ADMIN_CREATE_STATE => 'استان با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_STATE."_OP" => 'ویرایش استان',
    C::ADMIN_UPDATE_STATE => 'استان با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_STATE."_OP" => 'حذف استان',
    C::ADMIN_DELETE_STATE => 'استان با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_CREATE_CITY."_OP" => 'ایجاد شهر جدید',
    C::ADMIN_CREATE_CITY => 'شهر با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_CITY."_OP" => 'ویرایش شهر',
    C::ADMIN_UPDATE_CITY => 'شهر با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_CITY."_OP" => 'حذف شهر',
    C::ADMIN_DELETE_CITY => 'شهر با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_EDIT_POOL_COMMENT."_OP" => 'ویرایش نظر',
    C::ADMIN_EDIT_POOL_COMMENT => 'نظر با آی‌دی :id را ویرایش کرد.',

    C::ADMIN_APPROVED_POOL_COMMENT."_OP" => 'تایید نظر',
    C::ADMIN_APPROVED_POOL_COMMENT => 'نظر با آی‌دی :id را تایید کرد.',

    C::ADMIN_REJECT_POOL_COMMENT."_OP" => 'رد کردن نظر',
    C::ADMIN_REJECT_POOL_COMMENT => 'نظر با آی‌دی :id را رد کرد.',

    C::ADMIN_TO_TRASH_POOL_COMMENT."_OP" => 'انتقال نظر به زباله‌دان',
    C::ADMIN_TO_TRASH_POOL_COMMENT => 'نظر با آی‌دی :id را به زباله‌دان منتقل کرد.',

    C::ADMIN_RECOVER_POOL_COMMENT."_OP" => 'بازیابی نظر',
    C::ADMIN_RECOVER_POOL_COMMENT => 'نظر با آی‌دی :id را بازیابی کرد.',

    C::ADMIN_DELETE_POOL_COMMENT."_OP" => 'حذف نظر',
    C::ADMIN_DELETE_POOL_COMMENT => 'نظر با آی‌دی :id را حذف کرد.',

    C::ADMIN_UPDATE_THEME_CODE."_OP" => 'ویرایش هد و فوتر تم',
    C::ADMIN_UPDATE_THEME_CODE => '',

    C::ADMIN_CREATE_FACTOR."_OP" => 'ایجاد فاکتور جدید',
    C::ADMIN_CREATE_FACTOR => 'فاکتور جدید با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_FACTOR."_OP" => 'ویرایش فاکتور',
    C::ADMIN_UPDATE_FACTOR => 'فاکتور با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_FACTOR."_OP" => 'حذف فاکتور',
    C::ADMIN_DELETE_FACTOR => 'فاکتور با آی‌دی :id و نام :name را حذف کرد.',

    C::ADMIN_BLOG_CREATE_CATEGORY."_OP" => 'ایجاد دسته بندی بلاگ جدید',
    C::ADMIN_BLOG_CREATE_CATEGORY => 'دسته بندی بلاگ با نام :name و آی‌دی :id را ایجاد کرد.',

    C::ADMIN_BLOG_UPDATE_CATEGORY."_OP" => 'ویرایش دسته بندی بلاگ',
    C::ADMIN_BLOG_UPDATE_CATEGORY => 'دسته بندی با نام :name و آی‌دی :id را ویرایش کرد.',

    C::ADMIN_BLOG_DELETE_CATEGORY."_OP" => 'حذف دسته بندی بلاگ',
    C::ADMIN_BLOG_DELETE_CATEGORY => 'دسته بندی با نام :name و آی‌دی :id را حذف کرد.',

    C::ADMIN_BLOG_CREATE_TAG."_OP" => 'ایجاد تگ بلاگ جدید',
    C::ADMIN_BLOG_CREATE_TAG => 'تگ جدید با نام :name و آی‌دی :id را ایجاد کرد.',

    C::ADMIN_BLOG_UPDATE_TAG."_OP" => 'ویرایش تگ بلاگ',
    C::ADMIN_BLOG_UPDATE_TAG => 'تگ با نام :name و آی‌دی :id را ویرایش کرد.',

    C::ADMIN_BLOG_DELETE_TAG."_OP" => 'حذف تگ بلاگ',
    C::ADMIN_BLOG_DELETE_TAG => 'تگ با نام :name و آی‌دی :id را حذف کرد.',

    C::ADMIN_BLOG_CREATE_POST."_OP" => 'ایجاد پست بلاگ جدید',
    C::ADMIN_BLOG_CREATE_POST => 'پست جدید با عنوان :name و آی‌دی :id را ایجاد کرد.',

    C::ADMIN_BLOG_UPDATE_POST."_OP" => 'ویرایش پست بلاگ',
    C::ADMIN_BLOG_UPDATE_POST => 'پست با عنوان :name و آی‌دی :id را ویرایش کرد.',

    C::ADMIN_BLOG_DELETE_POST."_OP" => 'حذف پست بلاگ',
    C::ADMIN_BLOG_DELETE_POST => 'پست با عنوان :name و آی‌دی :id را حذف کرد.',

    C::ADMIN_BLOG_SETTING_UPDATE."_OP" => 'ویرایش تنظیمات بلاگ',
    C::ADMIN_BLOG_SETTING_UPDATE => '',

    C::ADMIN_CREATE_NEWSLETTER_USER."_OP" => 'ایجاد کاربر خبرنامه جدید',
    C::ADMIN_CREATE_NEWSLETTER_USER => 'کاربر خبرنامه جدید با ایمیل :email و آی‌دی :id را ایجاد کرد.',

    C::ADMIN_UPDATE_NEWSLETTER_USER."_OP" => 'ویرایش کاربر خبرنامه',
    C::ADMIN_UPDATE_NEWSLETTER_USER => 'کاربر خبرنامه با ایمیل :email و آی‌دی :id را ویرایش کرد.',

    C::ADMIN_DELETE_NEWSLETTER_USER."_OP" => 'حذف کاربر خبرنامه',
    C::ADMIN_DELETE_NEWSLETTER_USER => 'کاربر خبرنامه با ایمیل :email و آی‌دی :id را حذف کرد.',

    C::ADMIN_CREATE_REDIRECT_NOT_FOUND."_OP" => 'ایجاد ریدایرکت جدید',
    C::ADMIN_CREATE_REDIRECT_NOT_FOUND => 'ریدایرکت جدید با آی‌دی :id ایجاد کرد.',

    C::ADMIN_UPDATE_REDIRECT_NOT_FOUND."_OP" => 'ویرایش ریدایرکت',
    C::ADMIN_UPDATE_REDIRECT_NOT_FOUND => 'ریدایرکت با آی‌دی :id را ویرایش کرد.',

    C::ADMIN_DELETE_REDIRECT_NOT_FOUND."_OP" => 'حذف ریدایرکت',
    C::ADMIN_DELETE_REDIRECT_NOT_FOUND => 'ریدایرکت با آی‌دی :id را حذف کرد.',

    C::ADMIN_CREATE_ORGANIZATION."_OP" => 'ایجاد سازمان جدید',
    C::ADMIN_CREATE_ORGANIZATION => 'سازمان جدید با آی‌دی :id و نام :name را ایجاد کرد.',

    C::ADMIN_UPDATE_ORGANIZATION."_OP" => 'ویرایش سازمان',
    C::ADMIN_UPDATE_ORGANIZATION => 'سازمان با آی‌دی :id و نام :name را ویرایش کرد.',

    C::ADMIN_DELETE_ORGANIZATION."_OP" => 'حذف سازمان',
    C::ADMIN_DELETE_ORGANIZATION => 'سازمان با آی‌دی :id را حذف کرد.',

    C::ADMIN_UPDATE_RESERV_SETTINGS."_OP" => 'ویرایش تنظیمات رزرو'
];
