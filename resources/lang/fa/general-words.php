<?php

return [
    'pay_either' => 'همه',
    'not_paid' => 'پرداخت نشده',
    'paid' => 'پرداخت شده',
    'enter_either' => 'همه',
    'entered' => 'وارد شده',
    'not_entered' => 'وارد نشده',
    'gender_either' => 'همه',
    'males' => 'آقایان',
    'male' => 'آقا',
    'females' => 'بانوان',
    'female' => 'خانم',
    'cancel' => 'کنسل شده',
    'pending' => 'در حال بررسی',
    'partner_credit_event_'.C::PC_E_INCREASE_ADMIN => 'افزایش اعتبار توسط ادمین',
    'partner_credit_event_'.C::PC_E_DECREASE_ADMIN => 'عودت اعتبار توسط ادمین',
    'partner_credit_event_'.C::PC_E_DECREASE_GENERATE_CODE => 'کاهش اعتبار به دلیل تولید کد',
    'partner_credit_event_'.C::PC_E_SEND_SMS => 'کاهش اعتبار به دلیل ارسال اس ام اس',
];
