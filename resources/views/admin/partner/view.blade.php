@extends('admin.layout.main')

@section('title', 'مشاهده اطلاعات همکار')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">مشاهده اطلاعات همکار
            @include('admin.partner.navigation')
        </h1>
        <div class="panel panel-default">
            <div class="panel-heading">مشاهده اطلاعات همکار
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\PartnerController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <div class="text-left">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">عملیات‌ها <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="{{action('Admin\PartnerCreditController@getIncrease', $partner->partner_id)}}" class="text-right"><i class="fa fa-plus-square"></i> افزایش اعتبار</a></li>
                            <li><a href="{{action('Admin\PartnerCreditController@getDecrease', $partner->partner_id)}}" class="text-right"><i class="fa fa-minus-square"></i> عودت اعتبار</a></li>
                        </ul>
                    </div>
                </div>
                @if (!is_null($partner->avatar))
                <img src="{{URL::to(Manage::uploadDir('partners').$partner->partner_id.'.'.$partner->avatar)}}" class="img-thumbnail pull-left" style="margin: 10px 0;">
                @endif
                <h1>{{$partner->name}}</h1>
                <h3>اطلاعات کلی</h3>
                <table class="table table-bordered table-hover datatable">
                    <tr>
                        <th class="active">آی‌دی:</th>
                        <td class="en">{{$partner->partner_id}}</td>

                        <th class="active">نام:</th>
                        <td>{{"$partner->name $partner->last_name"}}</td>
                    </tr>
                    <tr>
                        <th class="active">نام کاربری</th>
                        <td>{{$partner->username}}</td>

                        <th class="active">وضعیت</th>
                        <td><span class="label label-{{checkStatus($partner->status, 'success', 'danger')}}">{{checkStatus($partner->status, 'فعال', 'غیر فعال')}}</span></td>
                    </tr>
                    <tr>
                        <th class="active">ایمیل:</th>
                        <td class="ltr">
                            <span class="en">{{$partner->email}}</span>
                        </td>

                        <th class="active">موبایل:</th>
                        <td class="ltr">
                            <span class="en">{{$partner->mobile}}</span>
                        </td>
                    </tr>
                    <tr>
                        <th class="active">اعتبار:</th>
                        <td class="success">
                            {{$partner->credit_admin}} ریال
                        </td>

                        <th class="active">کد API</th>
                        <td class="en">{{$partner->api_key}}</td>
                    </tr>
                </table>
                @if ($ips->count() > 0)
                <h3>آی‌پی‌ها</h3>
                <table class="table table-bordered table-hover">
                @foreach ($ips->chunk(4) as $ip)
                    <tr>
                    @foreach ($ip as $i)
                        <td>{{$i}}</td>
                    @endforeach
                    </tr>
                @endforeach
                </table>
                @endif
                @if ($creditEvents->count() > 0)
                <h3>
                    5 تغییرات آخر در اعتبار
                    <small><a href="{{action('Admin\PartnerCreditController@getIndex')}}?partner_id={{$partner->partner_id}}">مشاهده همه</a></small>
                </h3>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>تاریخ</th>
                            <th>نوع عملیات</th>
                            <th>مبلغ</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($creditEvents as $event)
                        <tr>
                            <td class="ltr text-right">{{$event->created_at_jalali}}</td>
                            <td>{{$event->code_message}}</td>
                            <td>{{$event->price_admin}} ریال</td>
                            <td><span class="label label-{{$event->payment_status_code}}">{{$event->payment_status_message}}</span></td>
                            <td>
                                <a href="{{action('Admin\PartnerCreditController@getView', $event->partner_credit_event_id)}}" class="btn btn-primary btn-xs" title="مشاهده"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
