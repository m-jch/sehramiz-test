@extends('admin.layout.main')

@section('title', 'تنظیمات اس ام اس')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">تنظیمات اس ام اس</h1>
        <div class="panel panel-default">
            <div class="panel-heading">تنظیمات اس ام اس</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif

                <form action="{{action('Admin\SmsController@postSettingSave')}}" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>هزینه هر اس ام اس</label>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('sms_price', $settings['sms_price']->value, ['class' => 'form-control en ltr p-sep-ltr']) !!}
                                <div class="input-group-addon">ریال</div>
                            </div>
                            {!! $errors->first('sms_price', "<p class='text text-danger'>:message</p>") !!}
                            <p class="bg-warning">هر اس ام اس 70 کاراکتر می‌باشد.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>متن پیام در زمان ارسال یک کد به کاربر</label>
                        </div>
                        <div class="col-md-5">
                            {!! Form::textarea('sms_generate_code_content', $settingsText['sms_generate_code_content']->value, ['class' => 'form-control', 'rows' => 6]) !!}
                            <p class="bg-warning">
                                کدهای قابل قبول: <b>{نام}</b>، <b>{نام خانوادگی}</b>، <b>{موبایل}</b>، <b>{ایمیل}</b>، <b>{کد}</b>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>متن پیام در زمان ارسال چند کد به کاربر</label>
                        </div>
                        <div class="col-md-5">
                            {!! Form::textarea('sms_generate_codes_content', $settingsText['sms_generate_codes_content']->value, ['class' => 'form-control', 'rows' => 6]) !!}
                            <p class="bg-warning">
                                کدهای قابل قبول: <b>{نام}</b>، <b>{نام خانوادگی}</b>، <b>{موبایل}</b>، <b>{ایمیل}</b>، <b>{شروع کدها}</b>
                                <b>{پایان کدها}</b>, <b>{کد}</b>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-info pull-left"><i class="fa fa-pencil"></i> ذخیره</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
