<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="MagnaIT">

    <title>@yield('title')</title>

    <link href="{{asset('admin-assets/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin-assets/dist/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('admin-assets/dist/css/main.min.css')}}" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('header')

    <style type="text/css" media="print">
        @page {
            size: auto;
            margin: 0mm;
        }
        body {
            background-color:#FFFFFF;
            border: solid 1px black ;
            margin: 0px;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                @yield('content')
            </div>
        </div>
    </div>
    <script>
        window.print();
    </script>
</body>
</html>
