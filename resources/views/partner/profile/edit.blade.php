@extends('pool-admin.layout.main')

@section('title', 'پروفایل')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">پروفایل</h1>
        <div class="panel panel-default">
            <div class="panel-heading">پروفایل</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <form action="{{action('PoolAdmin\ProfileController@postUpdate')}}" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>ایمیل</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('email', $poolAdmin->email, ['class' => 'form-control en ltr', 'required']) !!}
                            {!! $errors->first('email', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('name', $poolAdmin->name, ['class' => 'form-control']) !!}
                            {!! $errors->first('name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>نام خانوادگی</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('last_name', $poolAdmin->last_name, ['class' => 'form-control']) !!}
                            {!! $errors->first('last_name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>موبایل</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('mobile', $poolAdmin->mobile, ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('mobile', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>آواتار</label>
                        </div>
                        <div class="col-md-5">
                            <input name="avatar" type="file" class="form-control">
                            @if ($poolAdmin->avatar)
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="none-begard-remove-selected" data-name="avatar_removed" data-value="1">
                                        <li>
                                            <span class="remove"><a class="btn btn-xs btn-warning" href="javascript:void(0)"><i class="fa fa-times"></i></a></span>
                                            <span class="image"><img class="img-thumbnail" src="{{URL::asset(Manage::getUploadedDir('pools-admins').$poolAdmin->pool_admin_id.'.'.$poolAdmin->avatar)}}"></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endif
                            {!! $errors->first('avatar', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>رمز عبور</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::password('password', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                        <div class="col-md-12">
                            <p class="text-warning">اگر مایل به تغییر رمز نیستید، این فیلد را خالی رها کنید.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>تایید رمز عبور</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::password('password_confirmation', ['class' => 'form-control en ltr']) !!}
                            {!! $errors->first('password_confirmation', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-info pull-left"><i class="fa fa-pencil"></i> ذخیره</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
@include('admin.templates.begard')
<script>
    {{Helper::begardLastIndex()}}
    $(function() {
        begard.init();
    });
</script>
@stop
