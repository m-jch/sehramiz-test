@section('sidebar-cg-'.$category) active @stop

<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <div class="sidebar-profile pool-admin">
            <h4 class="title text-center"><b>پنل مدیریت مجموعه</b></h4>
            @if (Auth::guard('pool-admin')->user()->avatar)
            <div class="pull-right image">
                <img src="{{URL::to(Manage::getUploadedDir('pools-admins').Auth::guard('pool-admin')->id().'.'.Auth::guard('pool-admin')->user()->avatar)}}" class="img-circle">
            </div>
            @endif
            <h5 class="name pull-right" href="#">{{Auth::guard('pool-admin')->user()->name}}، خوش آمدید</h5>
        </div>
        <ul class="nav" id="side-menu">
            <li>
                <a class="@yield('sidebar-cg-dashboard')" href="{{action('PoolAdmin\DashboardController@getIndex')}}"><i class="fa fa-dashboard fa-fw"></i> داشبورد</a>
            </li>
            <li>
                <a class="@yield('sidebar-cg-reservations')" href="{{action('PoolAdmin\ReservationController@getIndex')}}"><i class="fa fa-star fa-fw"></i> رزرو شده‌ها</a>
            </li>
            <li>
                <a class="@yield('sidebar-cg-ledgers')" href="{{action('PoolAdmin\LedgerController@getIndex')}}"><i class="fa fa-dollar fa-fw"></i> معین‌ حساب‌</a>
            </li>
            <li>
                <a class="@yield('sidebar-cg-operators')" href="{{action('PoolAdmin\OperatorController@getIndex')}}"><i class="fa fa-users fa-fw"></i> اپراتورها</a>
            </li>
        </ul>
    </div>
</div>
