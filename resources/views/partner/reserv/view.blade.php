@extends('pool-admin.layout.main')

@section('title', 'اطلاعات رزرو')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">اطلاعات رزرو</h1>
        <div class="panel panel-default" id="pool-panel">
            <div class="panel-heading">اطلاعات رزرو
                <a class="btn btn-default btn-xs pull-left" href="{{action('PoolAdmin\ReservationController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th class="active">نام</th>
                            <td>{{$reservation->user->name}}</td>

                            <th class="active">کد واچر</th>
                            <td class="en">{{$reservation->voucher}}</td>
                        </tr>
                        <tr>
                            <th class="active">سانس</th>
                            <td>{{$reservation->saens_name}}</td>

                            <th class="active">جنسیت</th>
                            <td>{{Helper::printGender($reservation->gender)}}</td>
                        </tr>
                        <tr>
                            <th class="active">تاریخ سانس</th>
                            <td>{{to_j($reservation->start_datetime, false)}}</td>

                            <th class="active">زمان سانس</th>
                            <td>{{TicketHelper::displayTime($reservation)}}</td>
                        </tr>
                        <tr>
                            <th class="active">تعداد بلیت بزرگسال</th>
                            <td class="en">{{$reservation->count_adult}}</td>

                            <th class="active">تعداد بلیت خردسال</th>
                            <td class="en">{{$reservation->count_child}}</td>
                        </tr>
                        <tr>
                            <th class="active">قیمت بزرگسال</th>
                            <td class="warning"><span class="en">{{price($reservation->price_market_adult)->sep()}}</span> ریال</td>

                            <th class="active">قیمت خردسال</th>
                            <td class="warning"><span class="en">{{price($reservation->price_market_child)->sep()}}</span> ریال</td>
                        </tr>
                        <tr>
                            <th class="active">قیمت پس از تخفیف بزرگسال</th>
                            <td class="warning"><span class="en">{{price($reservation->price_buy_adult)->sep()}}</span> ریال</td>

                            <th class="active">قیمت پس از تخفیف خردسال</th>
                            <td class="warning"><span class="en">{{price($reservation->price_buy_child)->sep()}}</span> ریال</td>
                        </tr>
                        <tr>
                            <th colspan="2" class="active text-center">پرداختی به مجموعه</th>
                            <td colspan="2" class="success text-center"><span class="en">{{price($reservation->pay_to_pool)->sep()}}</span> ریال</td>
                        </tr>
                    </tbody>
                </table>
                <p>
                    <a href="{{action('PoolAdmin\ReservationController@getIndex')}}" class="btn btn-default">برگشت</a>
                </p>
            </div>
        </div>
    </div>
</div>
@stop
