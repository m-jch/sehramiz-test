<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;
use Sehramiz\Classes\EloquentGenderTrait;
use Sehramiz\Classes\EloquentTimestampJalaliTrait;

class PartnerUser extends Model
{
    use EloquentTimestampJalaliTrait, EloquentGenderTrait;

    protected $table = 'partners_users';

    protected $primaryKey = 'partner_user_id';
}
