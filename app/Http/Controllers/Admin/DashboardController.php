<?php

namespace Sehramiz\Http\Controllers\Admin;

use Date\Jalali;

class DashboardController extends Controller
{
    protected $category = 'dashboard';

    public function getIndex()
    {
        return $this->fire('admin.dashboard.index');
    }

    public function getAdminUi()
    {
        try {
            return [
                'status' => 1,
                'time' => $this->getTime(),
            ];
        } catch(\Exception $e) {
            return ['status' => 0];
        }
    }

    public function getTime()
    {
        return Jalali::now()->fa('Y/m/d H:i l');
    }
}
