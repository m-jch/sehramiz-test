<?php

namespace Sehramiz\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    /**
     * Check request must be redirect to back or index
     *
     * @param string index or wherever url
     * @param string back url
     * @return string
     */
    protected function checkStay($url, $backUrl = null)
    {
        if (checkStatus($this->request->get('stay'))) {
            return is_null($backUrl) ? back()->getTargetUrl() : $backUrl;
        }

        return $url;
    }
}
