<?php

namespace Sehramiz\Providers;

use Blade;
use Sehramiz\Classes\CustomValidator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->addRequiredBladeDirective();

        $this->app->validator->resolver(function($translator, $data, $rules, $messages)
		{
			return new CustomValidator($translator, $data, $rules, $messages);
		});
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function() {
            return base_path().'/public_html';
        });

        // Manage
        $this->app->singleton('sehramiz-manage', function()
        {
            return $this->app->make('Sehramiz\Classes\Manage');
        });

    }

    protected function addRequiredBladeDirective()
    {
        Blade::directive('required', function($expression) {
            return "<span class='text-danger'>*</span>";
        });
    }
}
