<?php

namespace Sehramiz\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Pool\Classes\Manage
 */
class Manage extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'sehramiz-manage'; }

}
