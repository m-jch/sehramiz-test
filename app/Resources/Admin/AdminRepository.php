<?php

namespace Sehramiz\Resources\Admin;

use Auth;
use File;
use Manage;
use Helper;
use Sehramiz\Models\Admin;
use Illuminate\Http\Request;
use Sehramiz\Resources\Repository;
use Illuminate\Database\DatabaseManager;

class AdminRepository extends Repository
{
    public function __construct
    (
        DatabaseManager $db,
        Request $request,
        Admin $admin
    )
    {
        $this->request = $request;
        $this->model = $admin;
        $this->db = $db;
    }

    /**
     * New user
     *
     * @return Sehramiz\Models\Admin
     */
    public function newAdmin()
    {
        $this->beginTransaction();

        return $this->setAdmin(new $this->model);
    }

    /**
     * Update user
     *
     * @return Sehramiz\Models\Admin
     */
    public function updateAdmin($adminId)
    {
        $this->beginTransaction();

        return $this->setAdmin($this->model->find($adminId));
    }

    public function setAdmin($admin)
    {
        $admin->name = $this->request->get('name');
        $admin->last_name = $this->request->get('last_name');
        if (!empty($this->request->get('password'))) $admin->password = bcrypt($this->request->get('password'));
        $admin->email = $this->request->get('email');
        $admin->username = $this->request->get('username');
        $admin->status = checkStatus($this->request->get('status'));
        $admin->mobile = Helper::mobileIdentifier($this->request->get('mobile'));

        if ($this->request->get('avatar_removed')) {
            $oldAdmin = $this->model->find($this->request->get('avatar_removed'), array('avatar'));
            if ($oldAdmin) {
                $this->deleteAvatar($admin->admin_id, $admin->avatar);
                $admin->avatar = null;
            }
        }

        if ($admin->admin_id == Auth::guard('admin')->id() && $admin->status != 1)
            throw new \Exception(trans('admin-messages.admin-disable-itsefl'));

        return $admin;
    }

    public function setAdminAvatar($admin)
    {
        if ($this->request->hasFile('avatar') && $this->request->file('avatar')->isValid()) {
            $ext = $this->request->file('avatar')->guessClientExtension();
            $this->request->file('avatar')->move(Manage::uploadDir('admins'), $admin->admin_id.'.'.$ext);

            $admin->avatar = $ext;
            $admin->save();
        }
    }

    public function deleteAvatar($adminId, $adminAvatar)
    {
        File::delete(Manage::uploadDir('admins').$adminId.'.'.$adminAvatar);
    }
}
