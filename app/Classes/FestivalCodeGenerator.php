<?php

namespace Sehramiz\Classes;

use C;
use Sehramiz\Models\Festival;
use Sehramiz\Models\FestivalCode;
use Sehramiz\Classes\PartnerCreditHolder;
use Sehramiz\Exceptions\NotEnoughCreditException;
use Sehramiz\Exceptions\FestivalCodesFinishedException;

class FestivalCodeGenerator
{
    public function __construct($festivalId, $partnerId)
    {
        $this->festivalId = $festivalId;
        $this->partnerId = $partnerId;
    }

    public function generateCodes($count, $userId, $partnerUserId)
    {
        $festival = Festival::find($this->festivalId, ['code_price', 'code_count_max']);
        $partnerCreditHolder = new PartnerCreditHolder($this->partnerId);

        $count = (int) $count;
        if ($count == 0) $count = 1;

        if (!$partnerCreditHolder->hasMinimumCredit($count * $festival->code_price))
            throw new NotEnoughCreditException;

        $generatedFestivalCodes = FestivalCode::where('festival_id', $this->festivalId)->count();
        if ($festival->code_count_max < ($generatedFestivalCodes + $count)) {
            throw new FestivalCodesFinishedException;
        }

        $codes = [];
        $partnerCreditEvents = [];
        for ($i = 0; $i < $count; $i++) {
            $code = new FestivalCode;
            $code->active = 1;
            $code->code = $this->generateFestivalCode();
            $code->user_id = $userId;
            $code->partner_user_id = $partnerUserId;
            $code->partner_id = $this->partnerId;
            $code->festival_id = $this->festivalId;
            $code->save();

            $codes[] = $code->code;

            $partnerCreditEvents[] = [
                'code' => C::PC_E_DECREASE_GENERATE_CODE,
                'price' => $festival->code_price,
                'related_id' => $code->festival_code_id,
            ];
        }

        $partnerCreditHolder->decreaseBunch($partnerCreditEvents);

        return $codes;
    }

    protected function generateFestivalCode()
    {
        $find = false;
        do {
            $code = $this->buildRandomCode(8);
            if (FestivalCode::where('festival_id', $this->festivalId)->where('code', $code)->count() > 0)
                $find = true;
            else
                $find = false;

        } while($find);

        return $code;
    }

    /**
     * @return string
     */
    public function buildRandomCode($length = 8)
    {
        $letters = 'qwertyuiopasdfghjklzxcvbnm0123456789';
        $code = '';
        for($i = 0; $i < $length; $i++) {
            $code .= $letters[mt_rand(0, strlen($letters) - 1)];
        }

        return $code;
    }
}
