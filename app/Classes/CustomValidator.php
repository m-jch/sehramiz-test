<?php

namespace Sehramiz\Classes;

use DateTime;
use Sehramiz\Classes\Helper;
use Illuminate\Validation\Validator;

class CustomValidator extends Validator
{
    /*
     * Validate more than of a field
     * For integer values
     */
    public function validateMoreThan($attribute, $value, $parameters)
    {
        $this->addReplacer('more_than', function($message, $attribute, $rule, $parameters) {
            $message = str_replace(':attribute', $attribute, $message);
            return str_replace(':target_attribute', $parameters[0], $message);
        });

        $this->requireParameterCount(2, $parameters, 'more_than');

        if ($value >= $parameters[1]) return true;

        return false;
    }

    /*
     * Validate more than of a field, if star passed return true
     * For integer values
     */
    public function validateMoreThanStar($attribute, $value, $parameters)
    {
        $this->addReplacer('more_than_star', function($message, $attribute, $rule, $parameters) {
            $message = str_replace(':attribute', $attribute, $message);
            return str_replace(':target_attribute', $parameters[0], $message);
        });

        $this->requireParameterCount(2, $parameters, 'more_than_star');

        if ($value == '*') return true;

        if ($value >= $parameters[1]) return true;

        return false;
    }

    /**
     * Validate DateTime
     */
    public function validateDateTime($attribute, $value, $parameters)
    {
        $this->addReplacer('date_time', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute', $attribute, $message);
        });

        $dateTime = en($value);
        $dateTime = DateTime::createFromFormat('Y/m/d H:i:s', $dateTime);
        if ($dateTime !== false) return true;

        return false;
    }

    /**
     * Validate Date
     */
    public function validateDate($attribute, $value)
    {
        $this->addReplacer('date', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute', $attribute, $message);
        });

        $dateTime = en($value);
        $dateTime = DateTime::createFromFormat('Y/m/d', $dateTime);
        if ($dateTime !== false) return true;

        return false;
    }

    /**
     * Validate Time
     */
    public function validateTime($attribute, $value)
    {
        $this->addReplacer('time', function($message, $attribute, $rule, $parameters) {
            return str_replace(':attribute', $attribute, $message);
        });

        $dateTime = en($value);
        $dateTime = DateTime::createFromFormat('H:i', $dateTime);
        if ($dateTime !== false) return true;

        return false;
    }

    /**
     * Validate price values separated by semi colon (,)
     */
    public function validatePrice($attribute, $value, $parameters)
    {
        return $this->validateNumeric($attribute, price($value)->int()->format(), $parameters);
    }

    /**
     * Validate price values separated by semi colon (,)
     */
    public function validatePriceMin($attribute, $value, $parameters)
    {
        $value = price($value)->int()->format();

        if (is_numeric($value)) {
            return $value >= $parameters[0];
        }

        return false;
    }

    /**
     * Validate price values separated by semi colon (,)
     */
    public function validatePriceMax($attribute, $value, $parameters)
    {
        $value = price($value)->int()->format();

        if (is_numeric($value)) {
            return $value <= $parameters[0];
        }

        return false;
    }

    /**
     * Validate mobile number
     */
    public function validateMobile($attribute, $value, $parameters)
    {
        $mobile = Helper::mobileIdentifier($value);
        if (is_null($mobile)) {
            return false;
        }

        return true;
    }
}
