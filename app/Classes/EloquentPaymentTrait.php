<?php

namespace Sehramiz\Classes;

use C;
use Illuminate\Support\Facades\Lang;

/**
 * For models have payment field
 */
trait EloquentPaymentTrait
{
    /**
     * Payment status message
     *
     * @return string
     */
    public function getPaymentStatusMessageAttribute()
    {
        switch ($this->payment) {
            case C::PAID:
                return Lang::get('general-words.paid');
                break;

            case C::NOT_PAID:
                return Lang::get('general-words.not_paid');
                break;
        }
    }

    /**
     * Payment status code such as success or danger
     *
     * @return string
     */
    public function getPaymentStatusCodeAttribute()
    {
        switch ($this->payment) {
            case C::NOT_PAID:
                return 'danger';

            case C::PAID:
                return 'success';
        }
    }
}
