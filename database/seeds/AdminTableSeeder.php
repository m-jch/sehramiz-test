<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = [
            array(
                'username' => 'admin',
                'name' => 'ادمین',
                'email' => 'email@test.com',
                'password' => bcrypt('123456'),
                'status' => 1
            )
        ];
        DB::table('admins')->insert($admins);
    }
}
