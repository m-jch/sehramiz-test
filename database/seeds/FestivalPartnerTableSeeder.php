<?php

use Illuminate\Database\Seeder;

class FestivalPartnerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $festivalsPartners = [
            array(
                'festival_id' => 1,
                'partner_id' => 1
            )
        ];
        DB::table('festivals_partners')->insert($festivalsPartners);
    }
}
