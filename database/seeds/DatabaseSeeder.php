<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        $this->call(SettingTextTableSeeder::class);
        $this->call(PartnerTableSeeder::class);
        $this->call(PartnerIpRestrictTableSeeder::class);
        $this->call(FestivalTableSeeder::class);
        $this->call(FestivalPartnerTableSeeder::class);

        Model::reguard();
    }
}
